#pragma once

#include <cstddef>

#include <vector>

namespace keytries
{
template<typename KeyType, typename ValueType> class value_node
{
public:

  value_node() = default;
  value_node(KeyType key) : _key(key) {}
  value_node(KeyType key, ValueType val) : _key(key), _val(val) {}

  template<typename ContainerType> void add_child(ContainerType container, ValueType val)
  {
    size_t c_index;

    if (!has_child(container.at(0)))
    {
      value_node<KeyType, ValueType> child(container.at(0));
      _children.emplace_back(child);
      c_index = _children.size() - 1; 
    }
    else
    {
      c_index = child_index(container.at(0)); 
    }

    if (container.begin() + 1 == container.end())
    {
      _children.at(c_index).value(val);
      return; 
    }

    ContainerType new_container(container.begin() + 1, container.end());
    
    _children.at(c_index).add_child(new_container, val); 
    
  }

  bool has_child(KeyType key) const
  {
    for (const auto& child : _children)
    {
      if (child.key() == key)
      {
	return true; 
      }
    }

    return false; 
  }

  value_node<KeyType, ValueType> get_child(KeyType key)
  {
    for (const auto& child : _children)
    {
      if (child.key() == key)
      {
	return child; 
      }
    }

    value_node<KeyType, ValueType> child;
    return child; 
  }
  
  void value(ValueType val)
  {
    _val = val; 
  }
  
  ValueType value() const
  {
    return _val; 
  }

  KeyType key() const
  {
    return _key; 
  }

  std::vector<value_node<KeyType, ValueType>> children() const
  {
    return _children; 
  }

private:

  size_t child_index(KeyType key)
  {
    size_t index = 0;

    for (const auto& child : _children)
    {
      if (child.key() == key)
      {
	return index; 
      }
      index = index + 1; 
    }

    return 0; 
  }
private:

  KeyType _key;
  ValueType _val;
  std::vector<value_node<KeyType, ValueType>> _children;  
  
};
}
