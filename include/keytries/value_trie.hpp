#pragma once

#include "value_node.hpp"

namespace keytries
{
template<typename KeyType, typename ValueType> class value_trie
{

public:
  value_trie() = default;

  template<typename ContainerType> void add(ContainerType& hash, ValueType val)
  {
    _root.add_child(hash, val); 
  }

  template<typename ContainerType> bool contains(ContainerType& hash) const
  {
    auto current = _root;

    for (const auto& key : hash)
    {
      if (current.has_child(key))
      {
	current = current.get_child(key);
      }
      else
      {
	return false; 
      }
    }
    return true; 
  }

  template<typename ContainerType> ValueType get(ContainerType& hash)
  {
    auto current = _root;

    for (const auto& key : hash)
    {
      current = current.get_child(key); 
    }
    return current.value(); 
  }
  
  
private:

  value_node<KeyType, ValueType> _root;
};
}
