#pragma once

#include <cstddef>

#include <vector> 

namespace keytries
{
template<typename T> class node
{
public:
  node() = default;
  node(T key) : _key(key) {}
		
  
public:

  template<typename ContainerType>void add_child(ContainerType& container)
  {
    if (container.empty()) { return; }

    size_t c_index;

    if (!has_child(container.at(0)))
    {
      node<T> child(container.at(0));
      _children.emplace_back(child);
      c_index = _children.size() - 1; 
    }
    else
    {
      c_index = child_index(container.at(0)); 
    }

    ContainerType tmp(container.begin() + 1, container.end());
    _children.at(c_index).add_child(tmp); 
  }

  // TODO: To be implemented
  void remove_child(const T key, bool only_remove_if_removeable=false)
  {
    for (auto it = _children.begin(); it != _children.end();)
    {
      if (it->key() == key)
      {
	bool rm = true; 
	if (only_remove_if_removeable && !it->removeable())
	{
	  rm = false; 
	}

	if (rm)
	{
	  _children.erase(it);
	}
	return; 
      }
    }
  }

  template<typename ContainerType> bool remove(const ContainerType& key)
  {
    if (key.size() == 2 && key.at(0) == _key && has_child(key.at(1)))
    {
      for (auto it = _children.begin(); it != _children.end();)
      {
	if (it->key() == key.at(1))
	{
	  if (it->removeable())
	  {
	    _children.erase(it);
	    if (_children.empty())
	    {
	      return true;
	    }
	    else
	    {
	      return false; 
	    }
	  }
	}  
      }
    }
    else if (key.size() > 2)
    {
      ContainerType subkey(key.begin() + 1, key.end());
      if (has_child(subkey.at(0)))
      {
	auto it = _children.begin();
	
	for (; it != _children.end();)
	{
	  if (it->key() == subkey.at(0))
	  {
	    if (it->remove(subkey))
	    {
	      _children.erase(it);
	      return removeable(); 
	    }
	  }
	}
	return false; 
      }
      else
      {
	return false; 
      }
    }
    return false; 
  }

  bool removeable() const
  {
    return _children.empty(); 
  }
  

  bool has_child(const T key) const
  {
    for (const node<T>& child : _children)
    {
      if (child.key() == key)
      {
	return true;
      }
    }
    return false; 
  }

  std::vector<node<T>> children() const
  {
    return _children; 
  }

  node<T> get_child(const T key) const
  {
    for (const node<T>& child : _children)
    {
      if (child.key() == key)
      {
	return child; 
      }
    }

    node<T> child; 
    return child; 
  }

  T key() const
  {
    return _key; 
  }
  
private:
  
  size_t child_index(const T key) const
  {
    size_t index = 0; 
    for (const node<T>& child : _children)
    {
      if (child.key() == key)
      {
	return index; 
      }
      index = index + 1; 
    }
    return 0; 
  }

private:

  T _key; 
  std::vector<node<T>> _children;  
};
}
