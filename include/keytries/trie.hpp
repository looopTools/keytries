#pragma once

#include "node.hpp"

#include <string>

namespace keytries
{
template<typename T>class trie
{

public:
  trie() = default;

  template<typename ContainerType> void add_hash(ContainerType& hash)
  {
    _root.add_child(hash); 
  }

  template<typename ContainerType> bool contains(ContainerType& hash)
  {
    node<T> current = _root;

    for (const auto& key : hash)
    {
      if (current.has_child(key))
      {
	current = current.get_child(key); 
      }
      else
      {
	return false;
      }
    }
    return true; 
  }

private:

  node<T> _root; 
};
}
