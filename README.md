# Keytries

Keytries implements a custom version of tries where no value is stored in the leave node.
The purpose of Keytries is not store a value in leave nodes, but to reduce the memory consumption for having keys in memory.
The thought application of Keytries is in particular in applications utilising data deduplication or generalised deduplication.
But other applications areas probably exists.
Keytries also includes a normal try tree called `value_trie`.

# Installation

Keytries is a C++ header only library.
You can simply copy the `keytries` folder (a subfolder in the `include` folder) to your library header path and that should be enough. 

# Usage

```c++
#include <keytries/trie.hpp>

#include <string>
#include <cassert>

int main(void)
{
    keytries::trie<char> trie;

    std::string hash = "keytries";
    trie.add_hash(hash);

    cassert(trie.contains(hash)); 
}
```

```c++
#include <keytries/value_trie.hpp>

#include <string>
#include <cassert>

int main(void)
{
    keytries::value_trie<char, int> trie;

    std::string hash = "keytries";
    int value = 32; 
    trie.add_hash(hash, value);

    cassert(trie.contains(hash));
    cassert(trie.get(hash) == value); 
}
```

# License

Keytries is released under the MIT license for more info see the [LICENSE](/LICENSE) file
