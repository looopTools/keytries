#include <keytries/value_trie.hpp>

#include <cstdint>
#include <vector>

#include <ctime>

#include <algorithm>

#include <iostream>

std::vector<uint8_t> generate_data(size_t size = 32)
{
  std::vector<uint8_t> data(size);
  std::generate(data.begin(), data.end(), rand);
  return data; 
}

int main(void)
{
  srand(static_cast<uint32_t>(time(0)));

  std::string key = "kajdkljaslkfklj";
  auto data = generate_data(); 
  
  keytries::value_trie<char, std::vector<uint8_t>> trie;
  trie.add(key, data);

  if (trie.contains(key))
  {
    std::cout << "[1] Success\n"; 
  }
  else
  {
    std::cout << "[1] Failure\n"; 
  }

  if (trie.get(key) == data)
  {
    std::cout << "[2] Success\n"; 
  }
  else
  {
    std::cout << "[2] Failure\n"; 
  }  
  
  
}
