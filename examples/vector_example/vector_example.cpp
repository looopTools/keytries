#include <keytries/trie.hpp>

#include <cstdint>

#include <vector>

#include <algorithm>

#include <iostream>

std::vector<uint8_t> generate_data(size_t size = 32)
{
  std::vector<uint8_t> data(size);
  std::generate(data.begin(), data.end(), rand);
  return data; 
}

int main(void)
{
  srand(static_cast<uint32_t>(time(0)));

  auto hash = generate_data();
  auto expected_a = hash;
  auto expected_b = hash;
  expected_b.emplace_back(1U);

  keytries::trie<uint8_t> t;
  t.add_hash(hash);

  if (t.contains(expected_a) && !t.contains(expected_b))
  {
    std::cout << "[1] Success\n"; 
  }
  else
  {
    std::cout << "[1] Failure\n"; 
  }

  t.add_hash(expected_b);

  if (t.contains(expected_a) && t.contains(expected_b))
  {
    std::cout << "[2] Success\n"; 
  }
  else
  {
    std::cout << "[2] Failure\n"; 
  }

  hash = generate_data();

  t.add_hash(hash);

  if (t.contains(expected_a) && t.contains(expected_b) && t.contains(hash))
  {
    std::cout << "[3] Success\n"; 
  }
  else
  {
    std::cout << "[3] Failure\n"; 
  }  
  
   
  

}
