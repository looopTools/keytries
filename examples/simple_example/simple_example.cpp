#include <keytries/trie.hpp>

#include <string>

#include <iostream>

int main(void)
{
  keytries::trie<char> t;

  std::string hash = "abcdedfghijklm";
  std::string expected_a = "abcdedfghijklm";
  std::string expected_b = "abcdedfghijklmk";  

  t.add_hash(hash);

  if (t.contains(expected_a) && !t.contains(expected_b))
  {
    std::cout << "[1] Success\n"; 
  }
  else
  {
    std::cout << "[1] Failure\n"; 
  }
  
  hash = "abcdedfghijklmk";
  
  t.add_hash(hash);

  if (t.contains(expected_a) && t.contains(expected_b))
  {
    std::cout << "[2] Success\n"; 
  }
  else
  {
    std::cout << "[2] Failure\n"; 
  }

  hash = "babcdedfghijklmk";
  std::string expected_c = "babcdedfghijklmk";
  
  t.add_hash(hash);

  if (t.contains(expected_a) && t.contains(expected_b) && t.contains(expected_c))
  {
    std::cout << "[3] Success\n"; 
  }
  else
  {
    std::cout << "[3] Failure\n"; 
  }       
}
